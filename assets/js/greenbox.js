$(document).ready(function($){
    
    //меню карточки товара
    $('.product-left:first-child').show();
    
    var menuItem = $('.product-right > nav > ul > li');
    menuItem.each(function(i, elem){
        $(elem).on('click', function(){
            menuItem.removeClass('active');
            $('.product-left').hide();
            var i = $(elem).index() + 1;
            $(elem).addClass('active');
            $('.product-left:nth-child(' + i + ')').show();
        });
    });
    //меню карточки товара
});